const express = require('express')
const Todo = require("../models/todo.model");
const toDoRoutes = express.Router();
app = express()

app.use('/todos', toDoRoutes);

// http://localhost:4000/todos/
// display or get all todo tasks from the database
toDoRoutes.route('/').get(function(request, response){
    Todo.find(function(err, todos){
        if(err){
            console.log(err)
        } else {
            response.json(todos)
        }
    })
})

// get a specific record based on id from the db
toDoRoutes.route('/:id').get(function(request, response){
    let id = request.params.id;
    Todo.findById(id, function(err, todo){
        response.json(todo)
    })
})

// http://localhost:4000/todos/add
// add a todo in db using post
toDoRoutes.route('/add').post(function(request, response){
    let todo = new Todo(request.body);
    todo.save()
        .then(todo =>{
            response.status(200).json({'todo': 'task is added successfully'});
        }) 
        .catch(err => {
            response.status(400).send('task is not added')           
        });
});

// update the todo task
toDoRoutes.route('/edit/:id').post(function(request, response){
    Todo.findById(request.param.id, function(err, todo){
        if(!todo){
            response.status(400).send('todo not found') 
        } else{
            todo.todo_description = request.body.todo_description;
            todo.todo_responsible = request.body.todo_responsible;
            todo.todo_priority = request.body.todo_priority;
            todo.todo_completed = request.body.todo_completed;

            todo.save()
            .then(todo => {
                response.json("Task is updated")
            })
            .catch(err => {
                response.status(400).send('Update failed') 
            })
        }
    })
})

module.exports = toDoRoutes;