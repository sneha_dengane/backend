const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')
const PORT = 4000

app.use(cors());
app.use(bodyParser.json());
const mongoose = require('./db.js')
const routes = require('./routes/routes.js')

app.use('/todos', routes)
app.listen(PORT, function(){
    console.log("Server is running on port no: " + PORT);
})
