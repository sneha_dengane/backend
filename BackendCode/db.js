const mongoose = require('mongoose');

// DB can be created or it will be created automatically when the data is posted
mongoose.connect('mongodb://localhost:27017/todos', (err)=>{
    if(!err){
        console.log('DB Connection Successful')
    } else{
        console.log('Error in Connection ' + err)
    }
})

module.exports = mongoose;